import { exportJWK, generateKeyPair, JWK, KeyLike, SignJWT } from 'jose';
import { v4 as uuid } from 'uuid';
import axios from 'axios';

// In order to make authenticated requests, both an access token and a DPoP (Demonstrating Proof-of-Possession) JWT
// MUST be provided in order prove that your service possess the the private key.
// See: https://datatracker.ietf.org/doc/html/draft-ietf-oauth-dpop
// See: https://datatracker.ietf.org/doc/html/rfc7517#section-4
async function createDpopJWT(privateKey: KeyLike, jwkPublicKey: JWK, method: string, url: string) {
  return new SignJWT({
      htu: url,
      htm: method.toUpperCase(),
      jti: uuid(),
  })
    .setProtectedHeader({
      alg: 'ES256',
      jwk: jwkPublicKey,
      typ: "dpop+jwt",
  })
      .setIssuedAt()
      .sign(privateKey);
}

async function main() {
  // Assume we already know our WebID, client id and secret
  const WEBID = 'https://oak-identity-provider-oak-develop.test.services.jtech.se/bnp/profile/card#me';
  const CLIENT_ID = 'bnp';
  const CLIENT_SECRET = 'vXpPxn6YkjeCVszJ';

  // Generate a keypair and derive a corresponding public key in JWK format(for embedding into JWT header).
  // Note: Store the private key for reuse in subsequent requests for protected resources.
  const keyPair = await generateKeyPair('ES256');
  const jwkPublicKey = await exportJWK(keyPair.publicKey);
  jwkPublicKey.alg = 'ES256';

  // Discover the token endpoint via the openid-configuration metadata
  const idpUrl = 'https://oak-identity-provider-oak-develop.test.services.jtech.se'; // Alternatively, can be discovered by dereferencing the WebID.
  const idpConfigEndpoint = `${idpUrl}/.well-known/openid-configuration`;
  const { data } = await axios.get(idpConfigEndpoint);
  const tokenEndpoint = data.token_endpoint;
  console.log('Step 1:', { tokenEndpoint });

  // Request an access token
  const credentials = `${encodeURIComponent(CLIENT_ID)}:${encodeURIComponent(CLIENT_SECRET)}`;
  const response1 = await axios.post(
    tokenEndpoint,
    'grant_type=client_credentials&scope=webid',
    {
      headers: {
        authorization: `Basic ${Buffer.from(credentials).toString('base64')}`,
        dpop: await createDpopJWT(keyPair.privateKey, jwkPublicKey, 'POST', tokenEndpoint),
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
  );
  const accessToken = response1.data.access_token;
  console.log('Step 2:', { accessToken });

  // Request a protected resource
  // In this case, we are requesting the access control list(ACL) for my WebID Profile Document.
  // Note: The ACL resource is represented using RDF/Turtle syntax.
  const protectedResourceURI = 'https://oak-identity-provider-oak-develop.test.services.jtech.se/bnp/profile/card.acl';
  const response2 = await axios.get(
    protectedResourceURI,
    {
      headers: {
        Authorization: `DPoP ${accessToken}`,
        dpop: await createDpopJWT(keyPair.privateKey, jwkPublicKey, 'GET', protectedResourceURI),
      },
    },
  );
  const protectedResource = response2.data;
  console.log('Step 3:', { protectedResource });

  // In order to request data from an end-user
  // redirect them to the Digital Wallet with
  // an encoded request payload in the url.
  const DIGITAL_WALLET_URL = 'https://digital-wallet-oak-develop.test.services.jtech.se';
  const PROVIDER_WEBID = 'https://oak-identity-provider-oak-develop.test.services.jtech.se/arbetsformedlingen/profile/card#me';
  const payload = {
    requestorWebId: WEBID,
    providerWebId: PROVIDER_WEBID,
    documentType: 'http://egendata.se/schema/core/v1#UnemploymentCertificate',
    returnUrl: 'http://bnp.com/return',
    purpose: 'Legal text goes here...',
  };
  const encoded = encodeURIComponent(Buffer.from(JSON.stringify(payload), 'utf8').toString('base64'));
  const link = `${DIGITAL_WALLET_URL}/?request=${encoded}`;
  console.log(`Step 4: Redirect user to Digital wallet at: ${link}`);

  // Next steps...?
  // Subscribe to for webhook notifications.
  // See: https://gitlab.com/arbetsformedlingen/individdata/oak/data-provider-app/-/blob/main/src/util/solid.ts#L102
  // Setup a server to handle incoming webhook messages
  // See: https://gitlab.com/arbetsformedlingen/individdata/oak/data-provider-app/-/blob/main/src/controller/webhookController.ts#L13
}

main();
